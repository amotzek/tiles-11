package lisp.server;
/*
 * Copyright (C) 2013, 2020 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import lisp.remote.server.ServerFactory;
import tiles.TilesFrameFactory;
import tiles.model.TilesModel;
import static javax.swing.SwingUtilities.invokeLater;
/**
 * Main Class that can start the Server
 *
 * Created by andreasm 15.10.13 13:46
 */
public class ServerApp
{
    private static final long NOW = 0L;
    private static final long THIRTY_SECONDS = 30000L;
    //
    public static void main(String[] args)
    {
        var logger = Logger.getLogger("lisp.server.ServerApp");
        //
        try
        {
            var server = ServerFactory.getServer(false);
            var protocol = server.getProtocol();
            var here = protocol.getHere();
            var name = here.getName();
            //
            if (headless(args))
            {
                logger.info("server " + name + " started in headless mode");
                waitForInterrupt();
                server.stop();
                //
                return;
            }
            //
            var listener = new WindowAdapter()
            {
                @Override
                public void windowClosing(WindowEvent e)
                {
                    server.stop();
                }
            };
            var timer = new Timer(true);
            var model = new TilesModel();
            model.createNewTiles();
            //
            invokeLater(() -> {
                var frame = TilesFrameFactory.createFrame(model);
                var task = new TimerTask()
                {
                    public void run()
                    {
                        invokeLater(() -> frame.setTitle(name + ' ' + server.getPlaces().size()));
                    }
                };
                timer.schedule(task, NOW, THIRTY_SECONDS);
                frame.addWindowListener(listener);
                frame.setVisible(true);
            });
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "cannot start", e);
        }
    }
    //
    private static boolean headless(String[] args)
    {
        return args != null && args.length == 1 && "-headless".equals(args[0]);
    }
    //
    private static void waitForInterrupt()
    {
        try
        {
            var forever = new Object();
            //
            synchronized (forever)
            {
                forever.wait();
            }
        }
        catch (InterruptedException e)
        {
            // do nothing
        }
    }
}