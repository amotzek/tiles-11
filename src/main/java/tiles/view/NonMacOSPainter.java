/*
 * Copyright (C) 1998, 2005 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package tiles.view;
/*
 * Created on 18.03.2005
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
/**
 * @author Andreasm
 */
final class NonMacOSPainter extends GenericPainter
{
    NonMacOSPainter(Graphics graphics)
    {
        super(graphics);
    }
    //
    public void paintTriangle(Color[] colors, int x, int y, int rotation)
    {
        var p = new Polygon();
        //
        switch (rotation % 4)
        {
            case 0:
                p.addPoint(x + HALF_SIZE + 1, y);
                p.addPoint(x + SIZE - 1, y);
                p.addPoint(x + SIZE - 1, y + HALF_SIZE - 1);
                graphics.setColor(colors[1]);
                //
                break;
            //
            case 1:
                p.addPoint(x + SIZE - 1, y + HALF_SIZE + 1);
                p.addPoint(x + SIZE - 1, y + SIZE - 1);
                p.addPoint(x + HALF_SIZE + 1, y + SIZE - 1);
                graphics.setColor(colors[3]);
                //
                break;
            //
            case 2:
                p.addPoint(x, y + HALF_SIZE + 1);
                p.addPoint(x, y + SIZE - 1);
                p.addPoint(x + HALF_SIZE - 1, y + SIZE - 1);
                graphics.setColor(colors[2]);
                //
                break;
            //
            case 3:
                p.addPoint(x, y);
                p.addPoint(x, y + HALF_SIZE - 1);
                p.addPoint(x + HALF_SIZE - 1, y);
                graphics.setColor(colors[0]);
                //
                break;
        }
        //
        graphics.fillPolygon(p);
    }
}