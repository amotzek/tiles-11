/*
 * Copyright (C) 1998, 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Tiles package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package tiles.view;
/*
 * Created on 18.03.2005
 */
import java.awt.Color;
/**
 * @author Andreasm
 */
public interface Painter
{
    int SIZE = 28;
    int HALF_SIZE = 14;
    //
    void paintBackground(Color color, int x, int y);
    //
    void paintOval(Color color, int x, int y);
    //
    void paintArc(Color[] colors, int x, int y, int rotation);
    //
    void paintTriangle(Color[] colors, int x, int y, int rotation);
    //
    void paintSquare(Color[] colors, int x, int y, int rotation);
    //
    void paintValue(Integer value, int x, int y);
}