package tiles.view;
/*
 * Copyright (C) 2013, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
/**
 * @author andreasm
 */
public class TilesFrame extends JFrame
{
    public TilesFrame(TilesPanel canvas)
    {
        super("Tiles");
        //
        var layout = new BorderLayout();
        setLayout(layout);
        add(canvas, BorderLayout.CENTER);
        setIconImage(readImage("iconimage.png"));
        setResizable(false);
        pack();
    }
    //
    private Image readImage(String filename)
    {
        var url = getClass().getResource(filename);
        var toolkit = Toolkit.getDefaultToolkit();
        var tracker = new MediaTracker(this);
        var image = toolkit.getImage(url);
        tracker.addImage(image, 0);
        //
        try
        {
            tracker.waitForAll();
        }
        catch (InterruptedException e)
        {
            return null;
        }
        //
        return image;
    }
}