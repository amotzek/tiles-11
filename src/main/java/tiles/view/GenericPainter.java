package tiles.view;
//
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
/**
 * Created by andreasm 19.10.14 07:43
 */
abstract class GenericPainter implements Painter
{
    final Graphics graphics;
    //
    GenericPainter(Graphics graphics)
    {
        this.graphics = graphics;
    }
    //
    public final void paintArc(Color[] colors, int x, int y, int rotation)
    {
        switch (rotation % 4)
        {
            case 0:
                graphics.setColor(colors[1]);
                graphics.fillArc(x + HALF_SIZE, y - HALF_SIZE + 1, SIZE - 2, SIZE - 2, 180, 90);
                //
                break;
            //
            case 1:
                graphics.setColor(colors[3]);
                graphics.fillArc(x + HALF_SIZE, y + HALF_SIZE, SIZE - 2, SIZE - 2, 90, 90);
                //
                break;
            //
            case 2:
                graphics.setColor(colors[2]);
                graphics.fillArc(x - HALF_SIZE + 1, y + HALF_SIZE, SIZE - 2, SIZE - 2, 0, 90);
                //
                break;
            //
            case 3:
                graphics.setColor(colors[0]);
                graphics.fillArc(x - HALF_SIZE + 1, y - HALF_SIZE + 1, SIZE - 2, SIZE - 2, 270, 90);
                //
                break;
        }
    }
    //
    public final void paintBackground(Color color, int x, int y)
    {
        graphics.setColor(color);
        graphics.fillRect(x, y, SIZE - 1,  SIZE - 1);
    }
    //
    public final void paintOval(Color color, int x, int y)
    {
        graphics.setColor(color);
        graphics.fillOval(x, y,  SIZE - 1,  SIZE - 1);
    }
    //
    public final void paintSquare(Color[] colors, int x, int y, int rotation)
    {
        switch (rotation % 4)
        {
            case 0:
                graphics.setColor(colors[1]);
                graphics.fillRect(x + HALF_SIZE, y, HALF_SIZE - 1, HALF_SIZE - 1);
                //
                break;
            //
            case 1:
                graphics.setColor(colors[3]);
                graphics.fillRect(x + HALF_SIZE, y + HALF_SIZE, HALF_SIZE - 1, HALF_SIZE - 1);
                //
                break;
            //
            case 2:
                graphics.setColor(colors[2]);
                graphics.fillRect(x, y + HALF_SIZE, HALF_SIZE - 1, HALF_SIZE - 1);
                //
                break;
            //
            case 3:
                graphics.setColor(colors[0]);
                graphics.fillRect(x, y, HALF_SIZE - 1, HALF_SIZE - 1);
                //
                break;
        }
    }
    //
    public final void paintValue(Integer value, int x, int y)
    {
        graphics.setColor(Color.BLACK);
        graphics.setFont(Font.decode("SansSerif-bold-9"));
        var metrics = graphics.getFontMetrics();
        var string = value.toString();
        x -= metrics.stringWidth(string) >> 1;
        y += metrics.getAscent() >> 1;
        graphics.drawString(value.toString(), x + HALF_SIZE, y + HALF_SIZE);
    }
}