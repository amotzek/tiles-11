package tiles.view;
/*
 * Copyright (C) 1998, 2005, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;
import tiles.model.TilesModel;
import tiles.model.TilesModelListener;
//
public final class TilesPanel extends JPanel implements TilesModelListener
{
    private final TilesModel model;
    //
    public TilesPanel(TilesModel model)
    {
        super();
        //
        this.model = model;
    }
    //
    public void connect()
    {
        model.addTilesModelListener(this);
    }
    //
    public void modelChanged()
    {
        repaint();
    }
    //
    @Override
    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }
    //
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(TilesModel.OUTER * Painter.SIZE - 1, TilesModel.OUTER * Painter.SIZE - 1);
    }
    //
    @Override
    public Dimension getMaximumSize()
    {
        return getPreferredSize();
    }
    //
    @Override
    public void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        var tiles = model.getTiles();
        //
        if (tiles == null) return;
        //
        var painter = createPainter(graphics);
        tiles.paint(painter);
    }
    //
    private static Painter createPainter(Graphics graphics)
    {
        var osname = System.getProperty("os.name").toLowerCase();
        var graphics2 = (Graphics2D) graphics;
        //
        if (osname.contains("mac"))
        {
            graphics2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //
            return new MacOSPainter(graphics2);
        }
        //
        if (osname.contains("windows"))
        {
            graphics2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        }
        else
        {
            graphics2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        //
        return new NonMacOSPainter(graphics2);
    }
}