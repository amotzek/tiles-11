package tiles;
/*
 * Copyright (C) 2014, 2017 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import tiles.controller.TilesFrameController;
import tiles.controller.TilesPanelController;
import tiles.controller.TimerController;
import tiles.model.TilesModel;
import tiles.view.TilesPanel;
import tiles.view.TilesFrame;
/**
 * Created by andreasm 19.10.14 08:09
 */
public final class TilesFrameFactory
{
    private TilesFrameFactory()
    {
    }
    //
    public static TilesFrame createFrame(TilesModel model)
    {
        // create views
        var panel = new TilesPanel(model);
        var frame = new TilesFrame(panel);
        // create controllers
        var framecontroller = new TilesFrameController(frame);
        var panelcontroller = new TilesPanelController(model, panel);
        var timercontroller = new TimerController(model);
        // register controllers as listeners for view events
        framecontroller.connect();
        panelcontroller.connect();
        timercontroller.connect();
        // register view as listener for model events
        panel.connect();
        //
        return frame;
    }
}