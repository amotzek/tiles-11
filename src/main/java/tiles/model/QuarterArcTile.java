/*
 * Copyright (C) 1998, 2005, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package tiles.model;
//
import java.util.Random;
import tiles.view.Painter;
//
final class QuarterArcTile extends QuarterTile
{
    QuarterArcTile(Random random)
    {
        super(random);
    }
    //
    public void paint(Painter painter, int x, int y)
    {
        paintBackground(painter, x, y);
        paintArc(painter, x, y, 0);
    }
}