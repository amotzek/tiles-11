/*
 * Copyright (C) 1998, 2005 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package tiles.model;
//
import java.util.Random;
//
abstract class TwoQuarterTile extends Tile
{
    TwoQuarterTile(Random random)
    {
        super(random);
        //
        switch (orientation)
        {
            case 0:
            case 2:
                color[3] = color[0];
                //
                break;
            //
            case 1:
            case 3:
                color[2] = color[1];
                //
                break;
        }
    }
}