/*
 * Copyright (C) 1998, 2005, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package tiles.model;
//
import java.awt.Color;
import java.util.Random;
import tiles.view.Painter;
//
abstract class Tile
{
    final Color[] color;
    final int orientation;
    //
    Tile(Random random)
    {
        super();
        //
        color = new Color[] { Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE };
        //
        for (int i = 0; i < 4; i++)
        {
            int j = random.nextInt(4);
            var c = color[i];
            color[i] = color[j];
            color[j] = c;
        }
        //
        orientation = random.nextInt(4);
    }
    //
    public abstract void paint(Painter painter, int x, int y);
    //
    final void paintBackground(Painter painter, int x, int y)
    {
        switch (orientation)
        {
            case 0:
                painter.paintBackground(color[0], x, y);
                //
                break;
            //
            case 1:
                painter.paintBackground(color[1], x, y);
                //
                break;
            //
            case 2:
                painter.paintBackground(color[3], x, y);
                //
                break;
            //
            case 3:
                painter.paintBackground(color[2], x, y);
                //
                break;
        }
    }
    //
    final void paintTriangle(Painter painter, int x, int y, int rotation)
    {
        painter.paintTriangle(color, x, y, orientation + rotation);
    }
    //
    final void paintSquare(Painter painter, int x, int y, int rotation)
    {
        painter.paintSquare(color, x, y, orientation + rotation);
    }
    //
    final void paintArc(Painter painter, int x, int y, int rotation)
    {
        painter.paintArc(color, x, y, orientation + rotation);
    }
    //
    final void paintOval(Painter painter, int x, int y)
    {
        painter.paintOval(Color.WHITE, x, y);
    }
    //
    final void paintValue(Painter painter, int x, int y, Integer value)
    {
        painter.paintValue(value, x, y);
    }
}