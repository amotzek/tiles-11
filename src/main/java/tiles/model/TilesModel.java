package tiles.model;
/*
 * Copyright (C) 2014, 2020 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by andreasm 18.10.14 16:12
 */
public class TilesModel
{
    public static final int OUTER = 12;
    public static final int INNER = 8;
    //
    private final LinkedList<TilesModelListener> listeners;
    private Tiles tiles;
    //
    public TilesModel()
    {
        super();
        //
        listeners = new LinkedList<>();
    }
    //
    public void addTilesModelListener(TilesModelListener listener)
    {
        listeners.add(listener);
    }
    //
    private void modelChanged()
    {
        for (var listener : listeners)
        {
            listener.modelChanged();
        }
    }
    //
    public synchronized void createNewTiles()
    {
        var tiles = new Tiles(OUTER, INNER);
        tiles.fill();
        this.tiles = tiles;
        modelChanged();
    }
    //
    public synchronized Tiles getTiles()
    {
        return tiles;
    }
}
