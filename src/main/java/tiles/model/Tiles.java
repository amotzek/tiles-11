package tiles.model;
/*
 * Copyright (C) 1998, 2005, 2013, 2020 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import java.util.Random;
import tiles.view.Painter;
/**
 * @author andreasm
 */
public final class Tiles
{
    private static final Random random = new Random();
    //
    private final int outer;
    private final int inner;
    private final Tile[][] tiles;
    private Tile savedtile;
    //
    Tiles(int outer, int inner)
    {
        super();
        //
        this.outer = outer;
        this.inner = inner;
        //
        tiles = new Tile[outer][outer];
    }
    //
    public void paint(Painter painter)
    {
        for (int i = 0; i < outer; i++)
        {
            for (int j = 0; j < outer; j++)
            {
                if (tiles[i][j] != null)
                {
                    int x = i * Painter.SIZE;
                    int y = j * Painter.SIZE;
                    tiles[i][j].paint(painter, x, y);
                }
            }
        }
    }
    //
    void fill()
    {
        placeRandomTiles();
        //
        while (improveTiles());
    }
    //
    private void placeRandomTiles()
    {
        int inner1 = (outer - inner) / 2;
        int inner2 = inner1 + inner;
        //
        for (int i = 0; i < outer; i++)
        {
            for (int j = 0; j < outer; j++)
            {
                if (i < inner1 || i >= inner2 || j < inner1 || j >= inner2) placeRandomTile(i, j);
            }
        }
    }
    //
    private boolean improveTiles()
    {
        boolean changed = false;
        //
        for (int i = 0; i < outer; i++)
        {
            for (int j = 0; j < outer; j++)
            {
                if (isEmpty(i, j)) continue;
                //
                if (replaceTile(i, j)) changed = true;
            }
        }
        //
        return changed;
    }
    //
    private boolean replaceTile(int i, int j)
    {
        int previous = mismatches(i, j);
        //
        if (previous == 0) return false;
        //
        saveTile(i, j);
        //
        for (int count = 0; count < 72; count++)
        {
            placeRandomTile(i, j);
            int next = mismatches(i, j);
            //
            if (next < previous) return true;
        }
        //
        restoreTile(i, j);
        //
        return false;
    }
    //
    private int mismatches(int i, int j)
    {
        var tile = tiles[i][j];
        int sum = 0;
        //
        try
        {
            var adjacent = tiles[i - 1][j];
            //
            if (adjacent.color[1] != tile.color[0]) sum++;
            if (adjacent.color[3] != tile.color[2]) sum++;
        }
        catch (Exception ignored)
        {
        }
        //
        try
        {
            var adjacent = tiles[i + 1][j];
            //
            if (adjacent.color[0] != tile.color[1]) sum++;
            if (adjacent.color[2] != tile.color[3]) sum++;
        }
        catch (Exception ignored)
        {
        }
        //
        try
        {
            var adjacent = tiles[i][j - 1];
            //
            if (adjacent.color[2] != tile.color[0]) sum++;
            if (adjacent.color[3] != tile.color[1]) sum++;
        }
        catch (Exception ignored)
        {
        }
        //
        try
        {
            var adjacent = tiles[i][j + 1];
            //
            if (adjacent.color[0] != tile.color[2]) sum++;
            if (adjacent.color[1] != tile.color[3]) sum++;
        }
        catch (Exception ignored)
        {
        }
        //
        return sum;
    }
    //
    private boolean isEmpty(int i, int j)
    {
        return tiles[i][j] == null;
    }
    //
    private void saveTile(int i, int j)
    {
        savedtile = tiles[i][j];
    }
    //
    private void restoreTile(int i, int j)
    {
        tiles[i][j] = savedtile;
    }
    //
    private void placeRandomTile(int i, int j)
    {
        tiles[i][j] = createTile();
    }
    //
    private static Tile createTile()
    {
        int r = random.nextInt( 5);
        //
        switch (r)
        {
            case 0:
                return new QuarterTriangleTile(random);
            //
            case 1:
                return new QuarterArcTile(random);
            //
            case 2:
                return new QuarterSquareTile(random);
            //
            case 3:
                return new TwoQuarterTriangleTile(random);
        }
        //
        return new TwoQuarterArcTile(random);
    }
}