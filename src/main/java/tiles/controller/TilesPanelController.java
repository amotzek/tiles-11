package tiles.controller;
/*
 * Copyright (C) 2013, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import tiles.model.TilesModel;
import tiles.view.TilesPanel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
/**
 * @author andreasm
 */
public final class TilesPanelController implements MouseListener
{
    private final TilesModel model;
    private final TilesPanel panel;
    //
    public TilesPanelController(TilesModel model, TilesPanel panel)
    {
        super();
        //
        this.model = model;
        this.panel = panel;
    }
    //
    public void connect()
    {
        panel.addMouseListener(this);
    }
    //
    public void mouseClicked(MouseEvent event)
    {
    }
    //
    public void mousePressed(MouseEvent event)
    {
    }
    //
    public void mouseReleased(MouseEvent event)
    {
        model.createNewTiles();
    }
    //
    public void mouseEntered(MouseEvent event)
    {
    }
    //
    public void mouseExited(MouseEvent event)
    {
    }
}