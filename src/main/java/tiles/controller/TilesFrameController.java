package tiles.controller;
/*
 * Copyright (C) 2013, 2014, 2020 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import tiles.view.TilesFrame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
/**
 * @author andreasm
 */
public final class TilesFrameController implements WindowListener
{
    private final TilesFrame frame;
    //
    public TilesFrameController(TilesFrame frame)
    {
        super();
        //
        this.frame = frame;
    }
    //
    public void connect()
    {
        frame.addWindowListener(this);
    }
    //
    public void windowOpened(WindowEvent event)
    {
    }
    //
    public void windowClosing(WindowEvent event)
    {
        frame.dispose();
    }
    //
    public void windowClosed(WindowEvent event)
    {
    }
    //
    public void windowIconified(WindowEvent event)
    {
    }
    //
    public void windowDeiconified(WindowEvent event)
    {
    }
    //
    public void windowActivated(WindowEvent event)
    {
    }
    //
    public void windowDeactivated(WindowEvent event)
    {
    }
}