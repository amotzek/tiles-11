package tiles.controller;
/*
 * Copyright (C) 2013, 2014 Andreas Motzek andreasm@qrst.de
 *
 * This file is part of the Tiles package.
 *
 * You can use, redistribute and/or modify this file
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 *
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
import tiles.model.TilesModel;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by andreasm 19.10.14 08:16
 */
public class TimerController implements ActionListener
{
    private static final int ONE_MIN = 60000;
    //
    private final TilesModel model;
    //
    public TimerController(TilesModel model)
    {
        super();
        //
        this.model = model;
    }
    //
    public void connect()
    {
        var timer = new Timer(ONE_MIN, this);
        timer.setRepeats(true);
        timer.start();
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        var thread = new Thread(model::createNewTiles);
        thread.start();
    }
}